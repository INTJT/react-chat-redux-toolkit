# React Chat (Redux Toolkit)

[Heroku](https://reacct-chat-redux-toolkit.herokuapp.com/)

## How-to run
* `git clone https://bitbucket.org/INTJT/react-chat-redux-toolkit.git`
* `yarn install`
* `yarn run install`
* create `server/.env` with `MONGODB_USER` and `MONGODB_PASSWORD` variables
* `yarn run start`

## How-to production
* `yarn run build`
* `yarn run production`
