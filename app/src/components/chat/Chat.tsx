import {useEffect} from "react";
import {MessageModel} from "../../models/messageModel";
import {UserModel} from "../../models/userModel";
import {Preloader} from "../preloader/Preloader";
import {Header} from "./header/Header";
import {MessageInput} from "./message-input/MessageInput";
import {Empty} from "./Empty";
import {MessageList} from "./message-list/MessageList";
import {useDispatch, useSelector} from "react-redux";
import {StoreState} from "../../redux/store";
import "./Chat.css";
import {EditModal} from "./edit-modal/EditModal";
import {setMessages} from "../../redux/messages/messagesCreators";
import {setUsers} from "../../redux/users/usersCreators";
import {setPreloader} from "../../redux/preloader/preloader";

type ChatProps = {
    url: string
}

export function Chat(props: ChatProps) {

    const state = useSelector((store: StoreState) => ({
        preloader: store.chat.preloader,
        isEmpty: store.chat.messages.length === 0
    }));
    const dispatch = useDispatch();

    useEffect(() => {

        interface RawMessage {
            id: string,
            userId: string,
            user: string,
            avatar: string,
            text: string,
            createdAt: string
            editedAt: string
        }

        function messageMapper(raw: RawMessage): MessageModel {
            return {
                id: raw.id,
                senderId: raw.userId,
                text: raw.text,
                liked: false,
                createdAt: new Date(raw.createdAt),
                editedAt: raw.editedAt ? new Date(raw.editedAt) : null
            }
        }

        (async function() {
            const response = await fetch(props.url);
            const rawMessages: RawMessage[] = await response.json();
            const messages: MessageModel[] = rawMessages.map(messageMapper);
            const users: UserModel[] = rawMessages.map(raw => ({
                id: raw.userId,
                name: raw.user,
                avatar: raw.avatar
            }));
            dispatch(setMessages(messages));
            dispatch(setUsers(users));
            dispatch(setPreloader(false));
        } ())

    }, [dispatch, props]);

    if(state.preloader) {
        return (
            <div className="chat loading">
                <Preloader />
            </div>
        );
    }
    else {
        const chatClasses = ["chat"];
        if(state.isEmpty) chatClasses.push("empty");
        return (
            <div className={chatClasses.join(" ")}>
                <Header />
                <div className="main">
                    {
                        !state.isEmpty ?
                            <MessageList />:
                            <Empty />
                    }
                </div>
                <MessageInput placeholder="Write a message..." />
                <EditModal />
            </div>
        )
    }

}

Chat.defaultProps = {
    url: "https://edikdolynskyi.github.io/react_sources/messages.json"
}
