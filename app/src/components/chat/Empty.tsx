import messageIcon from "../../icons/message.svg";

export function Empty() {
    return (
        <div className="empty centered">
            <div>
                <img src={messageIcon} width={100} height={100} alt=""/>
            </div>
            <h1>Empty chat</h1>
            <p>Type your first message</p>
        </div>
    );
}