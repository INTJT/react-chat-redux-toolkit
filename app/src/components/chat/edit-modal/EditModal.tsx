import {useDispatch, useSelector} from "react-redux";
import {StoreState} from "../../../redux/store";
import TextareaAutosize from "react-textarea-autosize";
import {useCallback, useEffect, useState} from "react";
import {hideModal} from "../../../redux/edit-modal/editModalCreators";
import {editMessage} from "../../../redux/messages/messagesCreators";
import "./EditModal.css";
import {ME} from "../../../models/userModel";

export function EditModal() {

    const {editable, visible} = useSelector((state: StoreState) => ({
        editable: state.chat.editModal ? state.chat.messages[state.chat.messages.length - 1] : undefined,
        visible: state.chat.editModal
    }));
    const dispatch = useDispatch();
    const [text, setText] = useState("");

    useEffect(() => {
        setText(editable ? editable.text : "");
    }, [editable]);

    const closeModal = useCallback(() => {
        dispatch(hideModal());
        setText("");
    }, [dispatch, setText]);

    const sendIfNotEmpty = useCallback(() => {
        if (editable
            && text !== editable.text
            && text.trim() !== ""
        ) {
            dispatch(editMessage(editable, text));
            closeModal();
        }
    }, [dispatch, closeModal, editable, text]);

    const classes = ["edit-message-modal", "modal-container"];
    if (editable) classes.push("modal-shown");

    return (
        <div className={classes.join(" ")} onClick={closeModal}>
            {
                visible && editable && editable?.senderId === ME.id ? (
                    <div className="modal-body" onClick={event => event.stopPropagation()}>
                        <h1 className="modal-title">Edit message</h1>
                        <TextareaAutosize
                            className="edit-message-input"
                            placeholder="Send"
                            value={text}
                            minRows={1} maxRows={5}
                            onChange={event => setText(event.target.value)}
                            onKeyDown={event => {
                                if (event.key === "Enter" && !event.shiftKey) {
                                    sendIfNotEmpty();
                                    event.preventDefault();
                                }
                            }}
                        />
                        <button className="edit-message-close" onClick={closeModal}>Cancel</button>
                        <button className="edit-message-button" onClick={sendIfNotEmpty}>Edit</button>
                    </div>
                ) : null
            }
        </div>
    );

}
