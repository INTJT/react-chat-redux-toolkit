import sendIcon from "../../../icons/send.svg";
import "./MessageInput.css";
import {useCallback, useState} from "react";
import TextareaAutosize from "react-textarea-autosize";
import {useDispatch, useSelector} from "react-redux";
import {writeMessage} from "../../../redux/messages/messagesCreators";
import {StoreState} from "../../../redux/store";
import {ME} from "../../../models/userModel";
import {addUser} from "../../../redux/users/usersCreators";

interface MessageInputProps {
    placeholder: string
}

export function MessageInput(props: MessageInputProps) {

    const users = useSelector((state: StoreState) => state.chat.users);

    const [text, setText] = useState("");
    const dispatch = useDispatch();

    const sendIfNotEmpty = useCallback(() => {
        if (text.trim() !== "") {
            if(!users.has(ME.id)) dispatch(addUser(ME));
            dispatch(writeMessage(text));
            setText("");
        }
    }, [dispatch, text, setText, users]);

    return (
        <div className="message-input">
            <TextareaAutosize
                className="message-input-text"
                placeholder={props.placeholder}
                value={text}
                minRows={1}
                maxRows={5}
                onChange={event => setText(event.target.value)}
                onKeyDown={event => {
                    if (event.key === "Enter" && !event.shiftKey) {
                        sendIfNotEmpty();
                        event.preventDefault();
                    }
                }}
            />
            <button className="message-input-button" onClick={sendIfNotEmpty}>
                <img className="icon" width={25} height={25} src={sendIcon} alt="Send"/>
            </button>
        </div>
    );

}

MessageInput.defaultProps = { placeholder: "Write a message..." }
