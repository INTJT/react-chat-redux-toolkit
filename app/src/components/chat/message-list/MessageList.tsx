import _ from "lodash";
import {DayGroup} from "./day-group/DayGroup";
import {useSelector} from "react-redux";
import {StoreState} from "../../../redux/store";

export function MessageList() {

    const messages = useSelector((state: StoreState) => state.chat.messages);

    const days = Object.entries(_.groupBy(messages, message => message.createdAt.toDateString()));

    return (
        <div className="message-list">
            {days.map(([day, messages]) => <DayGroup
                key={day}
                day={new Date(day)}
                messages={messages}
            />)}
        </div>
    );

}