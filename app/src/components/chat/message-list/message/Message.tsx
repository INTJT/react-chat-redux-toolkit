import {OwnMessageProps} from "./OwnMessage";
import {UserModel} from "../../../../models/userModel";
import moment from "moment";
import "./Message.css";
import likeIcon from "../../../../icons/like.svg";
import {useDispatch} from "react-redux";
import {toggleLikeMessage} from "../../../../redux/messages/messagesCreators";

interface AnotherMessageProps extends OwnMessageProps {
    sender: UserModel;
}

export function Message(props: AnotherMessageProps) {

    const dispatch = useDispatch();

    return (
        <div className="message">
            <img className="message-user-avatar" src={props.sender.avatar} width={50} height={50} alt=""/>
            <div className="message-body">
                <h1 className="message-user-name one-line">{props.sender.name}</h1>
                <div className="message-text">{props.message.text}</div>
                <button
                    className={props.message.liked ? "message-liked" : "message-like"}
                    onClick={() => dispatch(toggleLikeMessage(props.message))}
                >
                    <img className="icon" src={likeIcon} width={20} height={20} alt="" />
                </button>
                { props.message.editedAt ? <div className="message-edited one-line">edited</div> : null }
                <time className="message-time one-line">{moment(props.message.createdAt).format("HH:mm")}</time>
            </div>
        </div>
    );

}
