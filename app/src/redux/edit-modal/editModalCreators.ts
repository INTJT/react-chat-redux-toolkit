import {EditModalActionTypes, HideModalAction, ShowModalAction} from "./editModalActions";

export function showModal(): ShowModalAction {
    return { type: EditModalActionTypes.ShowModal };
}

export function hideModal(): HideModalAction {
    return { type: EditModalActionTypes.HideModal }
}
