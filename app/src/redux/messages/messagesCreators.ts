import {MessageModel} from "../../models/messageModel";
import {
    AddMessageAction,
    DeleteMessageAction,
    MessagesActionTypes,
    ReplaceMessageAction,
    SetMessageAction
} from "./messagesActions";
import {ME} from "../../models/userModel";
import {v4} from "uuid";

export function setMessages(messages: MessageModel[]): SetMessageAction {
    return {
        type: MessagesActionTypes.SetMessages,
        payload: messages
    }
}

export function addMessage(message: MessageModel): AddMessageAction {
    return {
        type: MessagesActionTypes.AddMessage,
        payload: message
    };
}

export function writeMessage(text: string): AddMessageAction {
    return addMessage({
        id: v4(),
        senderId: ME.id,
        text,
        liked: false,
        createdAt: new Date(),
        editedAt: null
    });
}

export function editMessage(message: MessageModel, text: string): ReplaceMessageAction {
    return {
        type: MessagesActionTypes.ReplaceMessage,
        payload: {
            old: message.id,
            new: {
                ...message,
                text,
                editedAt: new Date()
            }
        }
    }
}

export function toggleLikeMessage(message: MessageModel): ReplaceMessageAction {
    return {
        type: MessagesActionTypes.ReplaceMessage,
        payload: {
            old: message.id,
            new: {
                ...message,
                liked: !message.liked
            }
        }
    };
}

export function deleteMessage(message: MessageModel | string): DeleteMessageAction {
    return {
        type: MessagesActionTypes.DeleteMessage,
        payload: message
    }
}
