import {MessagesAction, MessagesActionTypes} from "./messagesActions";
import {MessageModel} from "../../models/messageModel";

export type MessagesState = MessageModel[];

export function messagesReducer(state: MessagesState = [], action: MessagesAction): MessagesState {
    switch (action.type) {

        case MessagesActionTypes.SetMessages:
            return action.payload;

        case MessagesActionTypes.AddMessage:
            return [...state, action.payload];

        case MessagesActionTypes.ReplaceMessage:
            const oldId = typeof action.payload.old === "string" ? action.payload.old : action.payload.old.id;
            return state.map(message => message.id !== oldId ? message : action.payload.new);

        case MessagesActionTypes.DeleteMessage:
            const deletedId = typeof action.payload === "string" ? action.payload : action.payload.id;
            return state.filter(message => message.id !== deletedId);

        default:
            return state;

    }
}
