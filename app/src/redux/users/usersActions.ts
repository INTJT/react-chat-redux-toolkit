import {UserModel} from "../../models/userModel";

export enum UsersActionTypes {
    SetUsers = "SET_USERS",
    AddUser = "ADD_USER",
    DeleteUser = "Delete_USER"
}

export type SetUsersAction = {
    type: UsersActionTypes.SetUsers,
    payload: UserModel[]
}

export type AddUserAction = {
    type: UsersActionTypes.AddUser,
    payload: UserModel
}

export type DeleteUserAction = {
    type: UsersActionTypes.DeleteUser,
    payload: UserModel | string
}

export type UsersAction = SetUsersAction | AddUserAction | DeleteUserAction;
