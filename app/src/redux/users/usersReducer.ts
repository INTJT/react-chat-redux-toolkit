import {UserModel} from "../../models/userModel";
import {UsersAction, UsersActionTypes} from "./usersActions";

export type UsersState = Map<string, UserModel>;

export function usersReducer(
    state: UsersState = new Map<string, UserModel>(),
    action: UsersAction
): UsersState {
    switch (action.type) {
        case UsersActionTypes.SetUsers:
            return new Map(action.payload.map(user => [user.id, user]));
        case UsersActionTypes.AddUser:
            return new Map([
                ...Array.from(state),
                [action.payload.id, action.payload]
            ]);
        case UsersActionTypes.DeleteUser:
            const deletedId = typeof action.payload === "string" ? action.payload : action.payload.id;
            return new Map(Array.from(state).filter(([id,]) => id !== deletedId));
        default:
            return state;
    }
}