import {Chat} from "./src/components/chat/Chat";
import {rootReducer} from "./src/redux/store";
export default {
    Chat,
    rootReducer
};
