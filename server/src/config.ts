import path from "path";
import dotenv from "dotenv";
dotenv.config();

const mongodbUserURI = encodeURIComponent(process.env.MONGODB_USER || "");
const mongodbPasswordURI = encodeURIComponent(process.env.MONGODB_PASSWORD || "");

export const PORT = process.env.PORT || "3000";
export const PRODUCTION = process.argv.includes("--production");
export const REACT_BUILD_PATH = path.join(__dirname, "../../app/build");
export const MONGODB_URI = `mongodb+srv://${mongodbUserURI}:${mongodbPasswordURI}@cluster0.zprcv.mongodb.net/react-chat?retryWrites=true&w=majority`;
export const JWT_SECRET = process.env.JWT_SECRET as string;
