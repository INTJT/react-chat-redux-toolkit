import {Express, Request, Response} from "express";
import usersRoutes from "./routes/usersRoutes";
import messageRoutes from "./routes/messagesRoutes";
import {HttpStatus} from "./httpStatus";

export enum Routes {
    Users = "/users",
    Messages = "/messages"
}

export function buildExpress(app: Express): void {
    app.use(Routes.Users, usersRoutes);
    app.use(Routes.Messages, messageRoutes);
    app.use(function (error: Error, req: Request, res: Response) {
        res.status(HttpStatus.InternalServerError).json({ error: "Something happened wrong, sorry" });
    });
}
