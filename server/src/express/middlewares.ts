import {NextFunction, Request, Response} from "express";
import {Types} from "mongoose";
import passport from "passport";
import {decrypt, jwtOptions} from "../helpers/jwtHelper";
import bodyParser from "body-parser";
import {HttpStatus} from "./httpStatus";
import {UsersService} from "../services/usersService";
import {IUser} from "../models/user";

export const jwtMiddleware = passport.authenticate("jwt", {session: false});
export const jsonMiddleware = bodyParser.json();
export const idMiddleware = function (req: Request, res: Response, next: NextFunction): void {
    if (Types.ObjectId.isValid(req.params.id)) next();
    else res.status(HttpStatus.BadRequest).json( { error: "Bad id" });
}

export const userMiddleware = async function (req: Request, res: Response, next: NextFunction): Promise<void> {
    const token = jwtOptions.jwtFromRequest(req) as string;
    req.query.userId = decrypt(token) as string;
    next();
};

export const adminMiddleware = async function (req: Request, res: Response, next: NextFunction): Promise<void> {
    const token = jwtOptions.jwtFromRequest(req) as string;
    const id = decrypt(token) as string;
    const user = await UsersService.getById(id) as IUser;
    if (!user.isAdmin) res.status(HttpStatus.Forbidden).json({error: "no admin privileges"});
    else next();
}
