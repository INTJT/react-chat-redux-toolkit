import {Router, Request, Response} from "express";
import {HttpStatus} from "../httpStatus";
import {MessagesService} from "../../services/messagesService";
import {idMiddleware, jsonMiddleware, jwtMiddleware, userMiddleware} from "../middlewares";
import {SocketIOAction, SocketIOService} from "../../services/socketIOService";

const router = Router();

router.use(
    jwtMiddleware,
    userMiddleware
);

router.get("/", async (req: Request, res: Response) => {
    const messages = await MessagesService.getAll();
    res.status(HttpStatus.Ok).json(messages.map(message => message.toJSON()));
});

router.get(
    "/:id",
    idMiddleware,
    async (req: Request, res: Response) => {
        const message = await MessagesService.getById(req.params.id);
        if(message) {
            res.status(HttpStatus.Ok).json(message.toJSON());
        }
        else {
            res.status(HttpStatus.NotFound).json({ error: "no found" });
        }
    }
);

router.post(
    "/",
    jsonMiddleware,
    async (req: Request, res: Response) => {
        const userId = req.query.userId as string;
        const text = req.body.text;
        const message = await MessagesService.writeMessage(userId, text);
        res.status(HttpStatus.Ok).json(message.toJSON());
        SocketIOService.emit(SocketIOAction.AddMessage, message.toJSON());
    }
);

router.patch(
    "/:id/edit",
    idMiddleware,
    jsonMiddleware,
    async (req: Request, res: Response) => {
        try {
            const userId = req.query.userId as string;
            const text = req.body.text;
            const message = await MessagesService.editMessage(req.params.id, userId, text);
            if(message) {
                res.status(HttpStatus.Ok).json(message.toJSON());
                SocketIOService.emit(SocketIOAction.UpdateMessage, message.toJSON());
            }
            else res.status(HttpStatus.NotFound).json({ error: "Not found" });
        }
        catch (error) {
            res.status(HttpStatus.Forbidden).json({ error: error.message });
        }
    }
);

router.patch(
    "/:id/like",
    idMiddleware,
    jsonMiddleware,
    async (req: Request, res: Response) => {
        try {
            const userId = req.query.userId as string;
            const message = await MessagesService.likeMessage(req.params.id, userId);
            if(message) {
                res.status(HttpStatus.Ok).json(message.toJSON());
                SocketIOService.emit(SocketIOAction.UpdateMessage, message.toJSON());
            }
            else res.status(HttpStatus.NotFound).json({ error: "Not found" });
        }
        catch (error) {
            res.status(HttpStatus.Forbidden).json({ error: error.message });
        }
    }
);

router.delete(
    "/:id",
    idMiddleware,
    jsonMiddleware,
    async (req: Request, res: Response) => {
        try {
            const userId = req.query.userId as string;
            if(await MessagesService.deleteMessage(req.params.id, userId)) {
                res.status(HttpStatus.Ok).json("Successfully deleted");
                SocketIOService.emit(SocketIOAction.DeleteMessage, req.params.id);
            }
            else res.status(HttpStatus.NotFound).json({ error: "Not found" });
        }
        catch (error) {
            res.status(HttpStatus.Forbidden).json({ error: error.message });
        }
    }
);

export default router;
