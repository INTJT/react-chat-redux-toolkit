import {Router, Request, Response} from "express";
import {generateToken} from "../../helpers/jwtHelper";
import {HttpStatus} from "../httpStatus";
import {UsersService} from "../../services/usersService";
import {adminMiddleware, idMiddleware, jsonMiddleware, jwtMiddleware} from "../middlewares";
import {SocketIOAction, SocketIOService} from "../../services/socketIOService";

const router = Router();

router.get("/",  async (req: Request, res: Response) => {
    const users = await UsersService.getAll();
    res.status(HttpStatus.Ok).json(users.map(user => user.toJSON()));
});

router.get(
    "/:id",
    idMiddleware,
    async (req: Request, res: Response) => {
        const user = await UsersService.getById(req.params.id);
        if(user) res.status(HttpStatus.Ok).json(user);
        res.status(HttpStatus.NotFound).json({ error: "no found" });
    }
);

router.post(
    "/auth",
    jsonMiddleware,
    async (req: Request, res: Response) => {
        const { email, password } = req.body;
        const user = await UsersService.getByEmail(email);
        if(user && user.isValidPassword(password)) {
            res.status(HttpStatus.Ok).send(generateToken(user));
        }
        else {
            res.status(HttpStatus.NotFound).json({ error: "bad login or password" });
        }
    }
);

router.post(
    "/",
    jwtMiddleware,
    jsonMiddleware,
    adminMiddleware,
    async (req: Request, res: Response) => {
        try {
            const user = req.body;
            const document = await UsersService.createUser(user);
            res.status(HttpStatus.Ok).json(document.toJSON());
            SocketIOService.emit(SocketIOAction.AddUser, document.toJSON());
        }
        catch (error) {
            res.status(HttpStatus.UnprocessableEntity).json({ error: error.message });
        }
    }
);

router.patch(
    "/:id",
    idMiddleware,
    jwtMiddleware,
    jsonMiddleware,
    adminMiddleware,
    async (req: Request, res: Response) => {
        try {
            const user = req.body;
            const document = await UsersService.updateUser(req.params.id, user);
            if(document) {
                res.status(HttpStatus.Ok).json(document.toJSON());
                SocketIOService.emit(SocketIOAction.UpdateUser, document.toJSON());
            }
            else res.status(HttpStatus.NotFound).json({ error: "not found" });
        }
        catch (error) {
            res.status(HttpStatus.UnprocessableEntity).json({ error: error.message });
        }
    }
);

router.delete(
    "/:id",
    idMiddleware,
    jwtMiddleware,
    adminMiddleware,
    async (req: Request, res: Response) => {
        const id = req.params.id;
        if(await UsersService.deleteUser(id)) {
            res.status(HttpStatus.Ok).send("Successfully deleted");
            SocketIOService.emit(SocketIOAction.DeleteUser, req.params.id);
        }
        else res.status(HttpStatus.NotFound).json({ error: "Not found" });
    }
);

export default router;
