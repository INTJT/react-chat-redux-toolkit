import mongoose from "mongoose";
import {MONGODB_URI} from "../config";

export function connectToDatabase(): Promise<typeof mongoose> {
    return mongoose.connect(MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
}
