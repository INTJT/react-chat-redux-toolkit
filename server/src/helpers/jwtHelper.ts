import passport from "passport";
import jwt, {JwtPayload} from "jsonwebtoken";
import {ExtractJwt, Strategy as JwtStrategy} from "passport-jwt";
import {IUserDocument, User} from "../models/user";
import {JWT_SECRET} from "../config";

export const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: JWT_SECRET
}

passport.use(new JwtStrategy(jwtOptions, (payload, done) => {
    User.findById(payload.sub)
        .then(
            user => done(null, user),
            error => done(error)
        )
}));

export function generateToken(user: IUserDocument): string {
    return jwt.sign({
        iss: "react-chat",
        sub: user._id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1)
    }, JWT_SECRET);
}

export function decrypt(token: string): string | undefined { return (jwt.verify(token, JWT_SECRET) as JwtPayload).sub; }
