import {PRODUCTION, PORT, REACT_BUILD_PATH} from "./config";
import http from "http";
import express from "express";
import socketIO from "socket.io";
import cors from "cors";
import {connectToDatabase} from "./helpers/databaseHelper";
import {buildExpress} from "./express";
import {buildSocketIO} from "./socket-io";

connectToDatabase().then(
    () => {

        const app = express();
        buildExpress(app);

        const server = new http.Server();

        const socket = new socketIO.Server(server);
        buildSocketIO(socket);

        /*
            in dev, server and react app has a different ports,
            but in heroku you can deploy app only on one port,
            so bind server and react app in production
         */
        if(PRODUCTION) app.use(express.static(REACT_BUILD_PATH));
        else app.use(cors());

        server.listen(PORT,  async () => {
            console.log(`Listen server on port ${PORT}`);
        });

    },
    error => console.error(error)
);
