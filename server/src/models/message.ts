import { Schema, Document, model } from "mongoose";

export const TEXT_CONSTRAINT = {
    maxLength: 2500
}

export interface IMessage {
    sender: string,
    text: string,
    createdAt: Date,
    editedAt?: Date,
    likes: string[]
}

export interface IMessageDocument extends IMessage, Document {
    edit(text: string): Promise<void>;
    like(userId: string): Promise<void>;
}

const MessageSchema = new Schema<IMessage>({
    sender: { type: Schema.Types.ObjectId, ref: "User", required: true },
    text: { type: String, required: true, trim: true, ...TEXT_CONSTRAINT },
    createdAt: { type: Date, required: true, default: Date.now },
    editedAt: { type: Date, required: false },
    likes: { type: [{ type: Schema.Types.ObjectId, ref: "User" }], default: [] }
});

MessageSchema.methods.edit = async function (text: string): Promise<void> {
    const message = this as IMessageDocument;
    message.text = text;
    message.editedAt = new Date();
    await message.save();
}

MessageSchema.methods.like = async function (userId: string): Promise<void> {
    const message = this as IMessageDocument;
    if(message.sender === userId) return;
    const index = message.likes.indexOf(userId);
    if(index !== -1) message.likes.splice(index, 1);
    else message.likes.push(userId);
    await message.save();
}

MessageSchema.methods.toJSON = function() {
    const message: IMessageDocument = this as IMessageDocument;
    return {
        id: message._id,
        sender: message.sender,
        text: message.text,
        createdAt: message.createdAt,
        editedAt: message.editedAt,
        likes: message.likes
    }
}

export const Message = model<IMessageDocument>("Message", MessageSchema);
