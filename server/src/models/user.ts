import { Schema, Document, model } from "mongoose";
import bcrypt from "bcrypt";

export const PASSWORD_CONSTRAINT = {
    minLength: 5,
    maxLength: 20
}

export interface IUser {
    login: string,
    email: string
    password: string,
    avatar?: string,
    isAdmin: boolean
}

export interface IUserDocument extends IUser, Document {
    isValidPassword(password: string): boolean
}

const UserSchema = new Schema<IUserDocument>({
    login: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, ...PASSWORD_CONSTRAINT },
    avatar: String,
    isAdmin: { type: Boolean, required: true, default: false },
});

UserSchema.methods.isValidPassword = function (password: string) {
    const user: IUser = this as IUser;
    return bcrypt.compareSync(password, user.password);
}

UserSchema.methods.toJSON = function() {
    const user: IUserDocument = this as IUserDocument;
    return {
        id: user._id,
        login: user.login,
        email: user.email,
        avatar: user.avatar,
        isAdmin: user.isAdmin
    };
}

UserSchema.pre(
    "save",
    function (next) {
        const user: IUserDocument = this as IUserDocument;
        if(user.isModified("password")) {
            user.password = bcrypt.hashSync(user.password, 10);
        }
        next();
    }
)

export const User = model<IUserDocument>("User", UserSchema);
