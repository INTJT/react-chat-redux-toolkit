import {IMessageDocument, Message, TEXT_CONSTRAINT} from "../models/message";

export class MessagesService {

    static async getMany(filter: object): Promise<IMessageDocument[]> {
        return Message.find(filter);
    }

    static async getAll(): Promise<IMessageDocument[]> {
        return MessagesService.getMany({});
    }

    static async get(filter: object): Promise<IMessageDocument | null> {
        return Message.findOne(filter);
    }

    static async getById(id: string): Promise<IMessageDocument | null> {
        return MessagesService.get({ _id: id });
    }

    static async writeMessage(senderId: string, text: string): Promise<IMessageDocument> {
        MessagesService.verifyText(text);
        const message = new Message({
            sender: senderId,
            text
        });
        await message.save();
        return message;
    }

    static async editMessage(id: string, userId: string, text: string): Promise<IMessageDocument | null> {
        MessagesService.verifyText(text);
        const message = await MessagesService.getById(id);
        if(message) {
            if(message.sender.toString() !== userId) throw new Error("This message isn't yours");
            await message.edit(text);
            return message;
        }
        else return null;
    }

    static async likeMessage(id: string, userId: string): Promise<IMessageDocument | null> {
        const message = await MessagesService.getById(id);
        if(message) {
            await message.like(userId);
            return message;
        }
        else return null;
    }

    static async deleteMessage(id: string, userId: string): Promise<boolean> {
        const message = await MessagesService.getById(id);
        if(message) {
            if(message.sender.toString() !== userId) throw new Error("This message isn't yours");
            await message.remove();
        }
        return Boolean(message);
    }

    private static verifyText(text: string) {
        if (text.length > TEXT_CONSTRAINT.maxLength) throw new Error("Too many symbols");
    }

}
