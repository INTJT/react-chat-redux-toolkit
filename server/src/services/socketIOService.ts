import socketIO from "socket.io";

export enum SocketIOAction {
    AddUser = "ADD_USER",
    UpdateUser = "UPDATE_USER",
    DeleteUser = "DELETE_USER",
    AddMessage = "ADD_MESSAGE",
    UpdateMessage = "UPDATE_MESSAGE",
    DeleteMessage = "DELETE_MESSAGE"
}

export class SocketIOService {
    
    private static io: socketIO.Server | null = null;
    
    static initialize(io: socketIO.Server) {
        SocketIOService.io= io;
    }
    
    static emit(action: SocketIOAction, ...args: any[]) {
        if(SocketIOService.io) {
            SocketIOService.io.emit(action, ...args);
        }
    }
    
}