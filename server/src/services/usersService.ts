import {IUser, IUserDocument, PASSWORD_CONSTRAINT, User} from "../models/user";

export class UsersService {

    static async getMany(filter: object): Promise<IUserDocument[]> {
        return User.find(filter);
    }

    static async getAll(): Promise<IUserDocument[]> {
        return UsersService.getMany({});
    }

    static async get(filter: object): Promise<IUserDocument | null> {
       return User.findOne(filter);
    }

    static async getById(id: string): Promise<IUserDocument | null> {
        return UsersService.get({ _id: id });
    }

    static async getByEmail(email: string): Promise<IUserDocument | null> {
        return UsersService.get({ email });
    }

    static async createUser(user: IUser): Promise<IUserDocument> {
        await UsersService.verifyUser(user);
        const document = new User(user);
        await document.save();
        return document;
    }

    static async updateUser(id: string, user: IUser): Promise<IUserDocument | null> {
        const document = await UsersService.getById(id);
        if(document) {
            if(user.email) {
                await UsersService.verifyEmail(user.email, document.id);
                document.email = user.email;
            }
            if(user.password) {
                UsersService.verifyPassword(user.password);
                document.password = user.password;
            }
            if(user.login) document.login = user.login;
            if(user.avatar) document.avatar = user.avatar;
            if(user.isAdmin) document.isAdmin = user.isAdmin;
            await document.save();
            return document;
        }
        else return null;
    }

    static async deleteUser(id: string): Promise<boolean> {
        const user = await UsersService.getById(id);
        if(user) await user.remove();
        return Boolean(user);
    }

    static async verifyUser(user: IUser) {
        if(!user.login) throw new Error("Must have login");
        if(!user.email) throw new Error("Must have email");
        if(!user.password) throw new Error("Must have password");
        await UsersService.verifyEmail(user.email, null);
        UsersService.verifyPassword(user.password);
    }

    private static async verifyEmail(email: string, currentId: string | null) {
        const users = await UsersService.getMany({ email });
        if(users.filter(user => user._id !== currentId).length !== 0) throw new Error("User w");
    }

    private static verifyPassword(password: string) {
        if(password.length < PASSWORD_CONSTRAINT.minLength || password.length > PASSWORD_CONSTRAINT.maxLength) {
            throw new Error(`Password must contains ${PASSWORD_CONSTRAINT.minLength}-${PASSWORD_CONSTRAINT.maxLength} characters`);
        }
    }

}
