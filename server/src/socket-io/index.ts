import socketIO from "socket.io";
import {SocketIOService} from "../services/socketIOService";

export function buildSocketIO(io: socketIO.Server): void {
    SocketIOService.initialize(io);
}
